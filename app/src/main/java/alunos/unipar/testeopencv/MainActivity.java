package alunos.unipar.testeopencv;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.features2d.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.features2d.KeyPoint;
import org.opencv.imgproc.Imgproc;
import org.opencv.ml.CvSVM;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    JavaCameraView camera;


    Mat matRGBa;
    Mat matCinza;

    private Bitmap bmpObjToRecognize, bmpScene, bmpMatchedScene;
    private double minDistance, maxDistance;
    private Scalar RED = new Scalar(255,0,0);
    private Scalar GREEN = new Scalar(0,255,0);
    private int matchesFound;

    BaseLoaderCallback callback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status){
                case BaseLoaderCallback.SUCCESS : {
                    camera.enableView();
                    break;
                }
                default:{
                    super.onManagerConnected(status);
                    break;
                }
            }

        }
    };

    boolean switchCinza = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        inicializarCamera();

        inicarOpenCV();

    }

    private void inicializarCamera(){
        camera = (JavaCameraView) findViewById(R.id.cameraView);
        camera.setVisibility(SurfaceView.VISIBLE);
        camera.setCvCameraViewListener(this);
    }


    @Override
    public void onCameraViewStarted(int width, int height) {
        matRGBa = new Mat(width, height, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        matRGBa.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {

        matRGBa  = inputFrame.rgba();

        if(switchCinza){
            matCinza = inputFrame.gray();
            return matCinza;
        }

        MatOfKeyPoint matOfKeyPoint = new MatOfKeyPoint();

        KeyPoint keyPoint = new KeyPoint();
        //matRGBa.get

        FeatureDetector featureDetector = FeatureDetector.create(FeatureDetector.ORB);
        //eatureDetector.detect();

        return matRGBa;
    }

    public void trocarParaCinza(View view){
        if(switchCinza){
            switchCinza = false;
        }else{
            switchCinza =  true;
        }
    }

    private void inicarOpenCV(){
        if(OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_13,this,callback)){
            Log.i(this.getClass().getName(),"Open CV iniciado com sucesso!");
            callback.onManagerConnected(BaseLoaderCallback.SUCCESS);
        }else{
            Log.i(this.getClass().getName(),"Problemas ao iniciar o OpenCV, tentando novamente.");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_13,this,callback);
        }
    }

    public void detectObject() {
        //Declaration
        Mat mObjectMat = new Mat();
        Mat mSceneMat = new Mat();
        MatOfDMatch matches = new MatOfDMatch();
        List<DMatch> matchesList;
        LinkedList<DMatch> good_matches = new LinkedList<>();
        MatOfDMatch gm = new MatOfDMatch();
        LinkedList<Point> objList = new LinkedList<>();
        LinkedList<Point> sceneList = new LinkedList<>();
        MatOfPoint2f obj = new MatOfPoint2f();
        MatOfPoint2f scene = new MatOfPoint2f();

        MatOfKeyPoint keypoints_object = new MatOfKeyPoint();
        MatOfKeyPoint keypoints_scene = new MatOfKeyPoint();
        Mat descriptors_object = new Mat();
        Mat descriptors_scene = new Mat();

        //Bitmap to MatUtils.bitmapToMat(bmpObjToRecognize, mObjectMat);
        Utils.bitmapToMat(bmpScene, mSceneMat);
        Mat img3 = mSceneMat.clone();

        //Use the FeatureDetector interface in order to find interest points/keypoints in an image.
        FeatureDetector fd = FeatureDetector.create(FeatureDetector.ORB);
        fd.detect(mObjectMat, keypoints_object );
        fd.detect(mSceneMat, keypoints_scene );

        //DescriptorExtractor
        //A descriptor extractor is an algorithm that generates a description of a keypoint that
        // makes this keypoint recognizable by a matcher. Famous descriptors are SIFT, FREAK...
        DescriptorExtractor extractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
        extractor.compute(mObjectMat, keypoints_object, descriptors_object );
        extractor.compute(mSceneMat, keypoints_scene, descriptors_scene );

        //DescriptorMatcher
        //Use a DescriptorMatcher for matching keypoint descriptors.
        DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
        matcher.match( descriptors_object, descriptors_scene, matches);

        //Calculate max and min distances between keypoints
        matchesList = matches.toList();
        for( int i = 0; i < descriptors_object.rows(); i++ )
        {
            Double dist = (double) matchesList.get(i).distance;
            if( dist < minDistance ) minDistance = dist;
            if( dist > maxDistance ) maxDistance = dist;
        }

        ////Draw only good matches
        for(int i = 0; i < descriptors_object.rows(); i++){
            if(matchesList.get(i).distance < 3*minDistance){
                good_matches.addLast(matchesList.get(i));
            }
        }
        gm.fromList(good_matches);
        matchesFound = good_matches.size();

        //Draw the matches
        Features2d.drawMatches(mObjectMat, keypoints_object, mSceneMat, keypoints_scene, gm, img3);

        //Localize the object & find the keypoints from the good matches
        List<KeyPoint> keypoints_objectList = keypoints_object.toList();
        List<KeyPoint> keypoints_sceneList = keypoints_scene.toList();

        for(int i = 0; i<good_matches.size(); i++){
            objList.addLast(keypoints_objectList.get(good_matches.get(i).queryIdx).pt);
            sceneList.addLast(keypoints_sceneList.get(good_matches.get(i).trainIdx).pt);
        }

        obj.fromList(objList);
        scene.fromList(sceneList);

        //Find homography between the scene and the object to recognize
        Mat hg = Calib3d.findHomography(obj, scene, Calib3d.RANSAC, minDistance);

        //Get the corners from the mObjectToDetectMat
        Mat obj_corners = new Mat(4,1, CvType.CV_32FC2);
        Mat scene_corners = new Mat(4,1,CvType.CV_32FC2);

        obj_corners.put(0, 0, new double[] {0,0});
        obj_corners.put(1, 0, new double[] {mObjectMat.cols(),0});
        obj_corners.put(2, 0, new double[] {mObjectMat.cols(),mObjectMat.rows()});
        obj_corners.put(3, 0, new double[] {0,mObjectMat.rows()});

        Core.perspectiveTransform(obj_corners, scene_corners, hg);

        Core.line(img3, new Point(scene_corners.get(0,0)), new Point(scene_corners.get(1,0)), new Scalar(0, 255, 0),4);
        Core.line(img3, new Point(scene_corners.get(1,0)), new Point(scene_corners.get(2,0)), new Scalar(0, 255, 0),4);
        Core.line(img3, new Point(scene_corners.get(2,0)), new Point(scene_corners.get(3,0)), new Scalar(0, 255, 0),4);
        Core.line(img3, new Point(scene_corners.get(3,0)), new Point(scene_corners.get(0,0)), new Scalar(0, 255, 0),4);


        //Convert Mat To Bitmap
        try {
            bmpMatchedScene = Bitmap.createBitmap(img3.cols(), img3.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(img3, bmpMatchedScene);
        }
        catch (CvException e){Log.d("Exception",e.getMessage());}
    }
}
